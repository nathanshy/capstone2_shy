let adminUser = localStorage.getItem('isAdmin')
let token = localStorage.getItem('token')

let cardFooter

let studentContainer = document.querySelector("#studentContainer")

if (adminUser == "false" || !adminUser) {
  alert(`You aren't authorized to view this page`)
} else {
  //get courseID from URL
  //courseId from the URL
  let params = new URLSearchParams(window.location.search)
  console.table(params)

  let courseId = params.get('courseId')
  console.log(courseId)
  //GET course info through fetch
  fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses/${courseId}`, {
    //use auth token
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
    .then(res => res.json())
    .then(data => {
      let studentData
      console.log(data)

      console.log(data.enrollees)

      if (data.enrollees < 1) {
        studentData = `No students enrolled in this course`
      } else {

        //find a way to do this
        studentData = data.enrollees.map(student => {
          //print the enrollees in cards
          //get enrollees name from ID
          //fetch from details UserController.details
          let enrolledOn = student.enrolledOn
          let userId = student.userId

          //do this another time, focus on the profile page first

          //2nd fetch
          fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/enroll/${userId}`, {
            headers: {
              Authorization: `Bearer ${token}`
            }
          })
            .then(res => res.json())
            .then(data => {

              //console.log(data)
              // let firstName = data.firstName
              // let lastName = data.lastName
              // return (`${firstName} ${lastName}`)
              studentContainer.innerHTML +=
                `
                  <div class="col-md-6 my-3" >
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title">Student Name: ${data.firstName} ${data.lastName}</h5>
                        <p class="card-text text-left">
                          Date Enrolled: ${enrolledOn}
                        </p>
                        <p class="card-text text-right">
                          Student ID: ${userId}
                        </p>
                      </div>
                      <div class="card-footer">
                      <a class="text-white"href="./removeStudent.html?courseId=${courseId}?userId=${userId}" value="${courseId}">
                        <button type="submit" class="btn btn-block btn-danger">                    
                          Remove Student
                        </button>
                        </a>
                      </div>
                    </div>
                  </div>
                `
            })

        }).join("")
      }
    })
    //catch error
    .catch((error) => {
      alert(`an error has occured`)
      console.log('Error', error)
    })
}
let token = localStorage.getItem('token')

//courseId from URL
let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')

//selecting the form
let form = document.querySelector('#editCourse')

// Details of course
fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses/${courseId}`, {
  headers: {
    Authorization: `Bearer ${token}`
  }
})
  .then(res => res.json())
  .then(data => {
    console.log(data.name)

    document.getElementById('courseName').value = data.name
    document.getElementById('coursePrice').value = data.price
    document.getElementById('courseDescription').value = data.description
  })
  .catch((error) => {
    alert(`an error has occured`)
    console.log('Error', error)
  })

//Form event listener
form.addEventListener("submit", (e) => {
  e.preventDefault()

  //selecting the input fields
  let courseName = document.querySelector('#courseName').value
  let coursePrice = document.querySelector('#coursePrice').value
  let courseDescription = document.querySelector('#courseDescription').value

  if (courseName == '' || coursePrice == '' || courseDescription == '') {
    alert(`Please give us all the info`)
  } else {
    //fetch to edit course
    fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses/${courseId}`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: courseName,
        description: courseDescription,
        price: coursePrice
      })
    })
      .then(res => res.json())
      .then(data => {
        //redirect user to course page on success alert in error
        console.log(data == true)
        if (data) {
          alert(`Your course will be updated shortly`)
          window.location.replace('./courses.html')
        } else {
          alert(`Something went wrong`)
        }
      })
      .catch((error) => {
        alert(`an error has occured`)
        console.log('Error', error)
      })
  }
})

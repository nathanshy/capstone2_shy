let loginForm = document.querySelector('#logInUser')

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();

  let userEmail = document.getElementById("userEmail").value
  let password = document.getElementById("password").value


  if (userEmail === '' || password === '') {
    alert(`Please input your email and password`)
  } else {
    fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "email": userEmail,
        "password": password
      })
    }).then(res => res.json())
      .then(data => {
        if (data.accessToken) {
          //succesful authentication will return JSON web token
          localStorage.setItem('token', data.accessToken)

          fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/details/`, {
            headers: {
              Authorization: `Bearer ${data.accessToken}`
            }
          })
            .then(res => res.json())
            .then(data => {
              console.log(data)
              //set the global user state to have properties containing authenticated user's ID and role
              localStorage.setItem("id", data._id)
              localStorage.setItem("isAdmin", data.isAdmin)
              //when writing code leave this out until everything works
              window.location.replace('./courses.html')
            })
        } else {
          alert(`invalid email/password`)
        }
      })
      .catch((error) => {
        alert(`an error has occured`)
        console.log('Error', error)
      })
  }
})



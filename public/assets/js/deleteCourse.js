let token = localStorage.getItem('token')

//get courseId from URL
let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

// 3. Create a fetch request to delete the specific course
fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses/${courseId}`, {
  method: 'DELETE',
  headers: {
    Authorization: `Bearer ${token}`,
    'Content-Type': 'application/json'
  }
})
  .then(res => res.json())
  .then(data => {
    alert(`course succesfully deleted`)
    window.location.replace('./courses.html')

  })
  .catch((error) => {
    alert(`an error has occured`)
    console.log('Error', error)
  })


// 	//Authorization is needed
// 4. Create a logic to redirect the user back to the courses page on success and alert the user if there's an error




// 4. Create a variable that will store the token of the user
//retrieve the JWT stored in our local storage for auth
let token = localStorage.getItem('token')

let createCourse = document.querySelector('#createCourse')
// ===============addCourse.js================
// Create the add course feature work

// 1. Select the form
// 2. Add an event listener to the form that will add a course on submit

createCourse.addEventListener("submit", (e) => {
  e.preventDefault()

  // 3. Select the input fields
  //prevent the page from reloading upon submitting the form
  let courseName = document.querySelector('#courseName').value
  let courseDescription = document.querySelector('#courseDescription').value
  let coursePrice = document.querySelector('#coursePrice').value

  // 5. Create a fetch request that will add the new course to the database
  //if the creation of new course is successful, redirect to courses.html and give an alert message "You have created a new course!"
  fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name: courseName,
      description: courseDescription,
      price: coursePrice
    })
  })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if (data == true) {
        alert(`Your course will be added soon`)
        window.location.replace('./courses.html')
      } else {
        alert(`Something went wrong`)
      }
    })
    .catch((error) => {
      alert(`an error has occured`)
      console.log('Error', error)
    })

})

//User token
let token = localStorage.getItem('token')
let id = localStorage.getItem('id')
let userAdmin = localStorage.getItem('isAdmin')
console.log(userAdmin)

console.log(token)

let params = new URLSearchParams(window.location.search)
console.table(params)

let courseId = params.get('courseId')
console.log(id)

//route to get course information
fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses/${courseId}`, {
  headers: {
    Authorization: `Bearer ${token}`
  }
})
  .then(res => res.json())
  .then(data => {
    console.log(data)
    document.querySelector('#courseName').innerHTML = data.name
    document.querySelector('#courseDesc').innerHTML = data.description
    document.querySelector('#coursePrice').innerHTML = data.price

    let enroll = document.querySelector('#enrollContainer')

    //user view

    if (userAdmin == "false" || !userAdmin) {
      //if return value is undefined, no user found
      let enrolled = data.enrollees.find(function (status, index) {
        console.log(status.userId)
        if (status.userId == id)
          return true
      })
      console.log(enrolled)

      //if user is enrolled they can't re-enroll in the course
      //not undefined == true
      if (enrolled != undefined) {
        enroll.innerHTML =
          `
          <button class="btn btn-block btn-success text-white enrollButton">
            Enrolled
          </button>
      `
        //if user isn't enrolled in the course clicking the button triggers enroll POST
      } else {
        console.log(`ID: ${id}`)
        console.log(`Token: ${token}`)
        enroll.innerHTML =
          `
          <button class="btn btn-block btn-dark text-white enrollButton">
            Enroll
          </button>
      `
        //event listener for Enroll button
        document.querySelector('#enrollContainer').addEventListener("click", (e) => {
          e.preventDefault()

          if (id == undefined) {
            window.location.replace(`./login.html`)
          }

          //fetch for enroll route
          fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/enroll`, {
            method: 'POST',
            headers: {
              Authorization: `Bearer ${token}`,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              courseId: courseId
            })
          })
            .then(res => res.json())
            .then(data => {
              console.log(data)
              if (data == true) {
                alert('Thanks for enrolling, see you soon')
                window.location.replace('./courses.html')
              } else {
                alert('something went wrong')
              }
            })
            .catch((error) => {
              alert(`an error has occured`)
              console.log('Error', error)
            })
        })
      }
      //if user is admin view students enrolled
    } else {
      enroll.innerHTML =
        `
      <button class="btn btn-block btn-primary text-white enrollButton">
        View Students
      </button>
      
      `
      //event listener to redirect to showStudents.html
      document.querySelector('#enrollContainer').addEventListener("click", (e) => {
        e.preventDefault()

        window.location.replace(`./showStudents.html?courseId=${courseId}`)
      })

    }
  })

//get user ID and token from localstorage
let isAdmin = localStorage.getItem('isAdmin')
let userId = localStorage.getItem('id')
let token = localStorage.getItem('token')

let profileContainer = document.querySelector('#profileContainer')

//pull ALL user info in container
fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/details`, {
  headers: {
    Authorization: `Bearer ${token}`
  }
})
  .then(res => res.json())
  .then(userData => {

    let courseCards

    console.log(userData.enrollments)

    //Main User Profile
    profileContainer.innerHTML =
      `
      <div class="container my-5">
        <div class="row">
          <div class="col-md-12">
            <div class="jumbotron text-left my-5">
              
            <h2>User Information</h2>

                <div class="jumbotron text-left my-5">
                    <table style="width:75%">
                      <tr>
                        <th>Name: </th>
                        <td>${userData.firstName} ${userData.lastName}</td>
                      </tr>

                      <tr>
                        <th>Phone Number: </th>
                        <td>${userData.mobileNo}</td>
                      </tr>

                      <tr>
                        <th>Email Address: </th>
                        <td> ${userData.email}</td>
                      </tr>

                      <tr>
                        <td><a href="./editUser.html">Edit Information</a></td>
                      </tr>

                    </table>
                      
                </div>
              </div>
          </div>
        </div>
      </div>


`



    let cards = document.querySelector('#cBody')
    userData.enrollments.map(userInfo => {
      if (!userInfo) alert(`Oops, something went wrong`)

      fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses`)
        .then(result => result.json())
        .then(courseData => {
          if (courseData.length < 1) alert(`No courses enrolled`)
          courseData.map(course => {
            if (course._id === userInfo.courseId) {

              let date = new Date(userInfo.enrolledOn);

              console.log(date);
              console.log(date.getDate());
              date = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
              return cards.innerHTML +=
                `
                <div class="col-md-6 my-3"">
                  <div class="card">
                    <div class="card-body text-left">
                      <h5 lass="card-title">Course Name: ${course.name}</h5>
                      <p class="card-text text-left">Enrolled On: ${date}</p>
                    </div>
                    <div class="card-footer">
                    </div>
                  </div>
                </div>
                `
            }
          }).join('')
        })


    })
  })
  .catch((error) => {
    alert(`an error occured`)
    console.log('Error', error)
  })





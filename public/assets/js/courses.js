let adminUser = localStorage.getItem("isAdmin")

let modalButton = document.querySelector("#adminButton")
let cardFooter;

if (adminUser == "false" || !adminUser) {
  modalButton.innerHTML = null
} else {
  modalButton.innerHTML =
    `
      <div class="col=md-2 offset-md-10">
        <a href='./addCourse.html' class="btn btn-block btn-primary">
          Add Course
        </a>
      </div>
    `
}

let container = document.querySelector("#coursesContainer")

//fetch by default is GET
fetch(`https://secure-hamlet-26500.herokuapp.com/api/courses`)
  .then(res => res.json())
  .then(data => {

    let courseData;

    if (data.length < 1) {
      courseData = `No courses available.`
    } else {
      //.map() method
      courseData = data.map(course => {
        //if the user is not an admin display when the course was created
        //and display the button "select course"
        //when the button is clicked, redirect to course.html
        if (adminUser == 'false' || !adminUser) {
          //if user is NOT enrolled in course SELECT COURSE button
          cardFooter =
            `
              <a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-dark text-white btn-block viewButton"> Select Course
              </a>
            `
          //if user IS enrolled in course green ENROLLED button. Match USER IDs
        } else {
          //else if the user is an admin display the edit & delete button
          //if course is active option to delete course
          if (course.isActive) {
            cardFooter =
              `
              <a href="./editCourse.html?courseId=${course._id}" value ={course._id} class="btn btn-success text-white btn-block editButton"> Edit Course
              </a>

              <a href="./deleteCourse.html?courseId=${course._id}" value ={course._id} class="btn btn-danger text-white btn-block deleteButton"> Disable Course
              </a>

              <a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block viewButton"> Select Course
              </a>
              `
            //else inactive  there is an option to reactivate the course
          } else {
            cardFooter =
              `
              <a href="./editCourse.html?courseId=${course._id}" value ={course._id} class="btn btn-success text-white btn-block editButton"> Edit Course
              </a>

              <a href="./reactivateCourse.html?courseId=${course._id}" value ={course._id} class="btn btn-warning text-white btn-block reactivateButton"> Reactivate Course
              </a>
              `

          }
        }
        //condition to show all ifAdmin
        //condition to show {isActive: true} to regular users
        if (adminUser == 'false' || !adminUser) {
          if (course.isActive) {
            return (
              `
              <div class="col-md-6 my-3">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">${course.name}</h5>
                    <p class="card-text text-left">
                      ${course.description}
                    </p>
                    <p class="card-text text-right">
                      &#8369; ${course.price}
                    </p>
                  </div>
                  <div class="card-footer">
                    ${cardFooter}
                  </div>
                </div>	
              </div>
            `
            )
          }
        } else {
          return (
            `
            <div class="col-md-6 my-3">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">${course.name}</h5>
                  <p class="card-text text-left">
                    ${course.description}
                  </p>
                  <p class="card-text text-right">
                    &#8369; ${course.price}
                  </p>
                </div>
                <div class="card-footer">
                  ${cardFooter}
                </div>
              </div>	
            </div>
            `
          )

        }
      }).join("")
      //since the collection is an array, we can use join to indicate the separator of each element to replace the comma
    }
    container.innerHTML = courseData
  })
  .catch((error) => {
    alert(`an error has occured`)
    console.log('Error', error)
  })
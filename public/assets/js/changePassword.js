//get user ID and token from localstorage

let userId = localStorage.getItem('id')
let token = localStorage.getItem('token')

let passwordChangeForm = document.querySelector('#changePasswordForm')

passwordChangeForm.addEventListener("submit", (e) => {

  e.preventDefault();

  //get value from text boxes
  let originalPassword = document.querySelector('#originalPassword').value
  let password1 = document.querySelector('#password1').value
  let password2 = document.querySelector('#password2').value

  if (originalPassword == "" || password1 == "" || password2 == "") {
    alert(`Please fill in the form`)
  }

  if (password1 === password2) {

    //pull ALL user info in container
    fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/change-password`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        password: originalPassword,
        newPassword: password1
      })
    })
      .then(res => res.json())
      .then(data => {
        //redirect user to profile page on success alert in error
        console.log(data)
        if (data == true) {
          alert(`Your password has been changed`)
          window.location.replace('./profile.html')
        } else if (data === 1) {
          alert(`Incorrect password`)
        } else {
          alert(`Incorrect Password`)
        }
      })
      .catch((error) => {
        alert(`an error has occured`)
        console.log('Error', error)
      })
  } else {
    return alert(`Passwords do not match`)
  }

})
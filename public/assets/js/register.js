//form
let registerForm = document.querySelector("#registerUser")

//clicking register
registerForm.addEventListener("submit", (e) => {
	e.preventDefault() //if the event doesn't handle properly this prevents the default action. Also prevents auto page refresh

	let firstName = document.getElementById("firstName").value
	let lastName = document.getElementById("lastName").value
	let mobileNumber = document.getElementById("mobileNumber").value
	let userEmail = document.getElementById("userEmail").value
	let password1 = document.getElementById("password1").value
	let password2 = document.getElementById("password2").value

	if ((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNumber.length === 11)) {
		//Check database
		//Check for duplicate email in database first
		//url where we can get the duplicate routes in our server
		//asynchronously load contents of the URL
		fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				"email": userEmail
			})
		})
			//returns a promise that resolves when the result is loaded
			.then(res => res.json()) //call this funciton res.json() when the result is loaded
			.then(data => {
				console.log(data)
				//if true(duplicate email) return alert
				if (data === true) {
					return alert(`email address in use`)
				} else {
					//if no duplicates fouund
					//get the routes for registration in our server
					fetch(`https://secure-hamlet-26500.herokuapp.com/api/users`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							"firstName": firstName,
							"lastName": lastName,
							"email": userEmail,
							"mobileNo": mobileNumber,
							"password": password1
						})
					}).then(res => {
						return res.json()
					}).then(data => {
						console.log(data)

						//if registration is succesful
						if (data === true) {
							alert(`registered succesfully`)
							//redirect to login page
							window.location.replace("./login.html")
						} else {
							//error occured in registration
							alert(`Something went wrong`)
						}
					})
				}
			})
			.catch((error) => {
				alert(`an error has occured`)
				console.log('Error', error)
			})
	} else {
		alert(`Something Went Wrong, Please try again`)
	}
})
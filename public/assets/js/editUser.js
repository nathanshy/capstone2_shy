//get user ID and token from localstorage

let userId = localStorage.getItem('id')
let token = localStorage.getItem('token')

let editForm = document.querySelector('#editForm')

editForm.addEventListener("submit", (e) => {

  e.preventDefault();

  //get value from text boxes
  let editFirstName = document.querySelector('#firstName').value
  let editLastName = document.querySelector('#lastName').value
  let editMobileNo = document.querySelector('#mobileNo').value
  let editEmail = document.querySelector('#userEmail').value

  fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/email-exists`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "email": editEmail
    })
  })
    //returns a promise that resolves when the result is loaded
    .then(res => res.json()) //call this funciton res.json() when the result is loaded
    .then(data => {
      console.log(data)
      //if true(duplicate email) return alert
      if (data === true) {
        return alert(`email address in use`)
      } else {
        //if no duplicates fouund
        //get the routes for edit detailss in the server

        //pull ALL user info in container
        //https://secure-hamlet-26500.herokuapp.com/api/users/details
        fetch(`https://secure-hamlet-26500.herokuapp.com/api/users/details`, {
          method: 'PUT',
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            firstName: editFirstName,
            lastName: editLastName,
            email: editEmail,
            mobileNo: editMobileNo
          })
        })
          .then(res => res.json())
          .then(data => {
            //redirect user to profile page on success alert in error
            console.log(data == true)
            if (data) {
              alert(`Your information has been saved`)
              window.location.replace('./profile.html')
            } else {
              alert(`Something went wrong`)
            }
          })
          .catch((error) => {
            alert(`an error has occured`)
            console.log('Error', error)
          })
      }

    })
    .catch((error) => {
      alert(`an error has occured`)
      console.log('Error', error)
    })

})

